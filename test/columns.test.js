describe("probar la sistema de creación de columnas",() => {
    it('deberia de crear columna', (done) => {
        superTest(app).post('/columns').send({'titulo': 'column'}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de crear una columna', (done) => {
        superTest(app).post('/columns').send({'titulo': 'column'}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar la sistema de actualización de columnas",() => {
    it('deberia de remplazar la columna', (done) => {
        superTest(app).put(`/columns/6387db50b594066dfcf5c7af`).send({'titulo': 'columns'}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de remplazar la columna', (done) => {
        superTest(app).put('/columns/6387db50b594066dfcf5c7af').send({'titulo': 'column'}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('deberia de actualizar la columna', (done) => {
        superTest(app).patch('/columns/6387db50b594066dfcf5c7af').send({'titulo': 'columns2'}).expect(200)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('no deberia de actualizar la columna', (done) => {
        superTest(app).patch('/columns/6387db50b594066dfcf5c7af').send({'titulo': 'columnsw'}).expect(200)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar la sistema de destrucción de columnas",() => {
    it('deberia de borrar una columna', (done) => {
        superTest(app).delete('/columns/6387db50b594066dfcf5c7af').send().expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de borrar una columna', (done) => {
        superTest(app).post('/columns/6387d1572c3b6a2641d97574').send().expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});