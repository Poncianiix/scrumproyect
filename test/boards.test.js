describe("probar el sistema de creación de tableros",() => {
    it('deberia de crear tablero', (done) => {
        superTest(app).post('/boards').send({'name': 'column', 'columns': []}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de crear un tablero', (done) => {
        superTest(app).post('/boards').send({'name': 'column', 'columns': ['1234453453']}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar el sistema de actualización de tableros",() => {
    it('deberia de remplazar el tablero', (done) => {
        superTest(app).put(`/boards/6387d61d969922171c42d1bb`).send({'name': 'columns', 'columns': ['1234453453']}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de remplazar el tablero', (done) => {
        superTest(app).put('/boards/6387d61d969922171c42d1bb').send({'name': 'column', 'columns': '1234453453'}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('deberia de actualizar el tablero', (done) => {
        superTest(app).patch('/boards/6387d61d969922171c42d1bb').send({'name': 'columns2', 'columns': ['1234453453']}).expect(200)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('no deberia de actualizar el tablero', (done) => {
        superTest(app).patch('/boards/6387d61d969922171c42d1bb').send({'name': 'columnsw', 'columns': '1234453453'}).expect(200)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar el sistema de destrucción de tableros",() => {
    it('deberia de borrar tablero', (done) => {
        superTest(app).delete('/boards/6387d61d969922171c42d1bb').send().expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de borrar un tablero', (done) => {
        superTest(app).post('/boards/6387d1572c3b6a2641d97574').send().expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});