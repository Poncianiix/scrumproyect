describe("probar el sistema de creación de tarjetas",() => {
    it('deberia de crear tarjeta', (done) => {
        superTest(app).post('/boards').send({'texto': 'texto'}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de crear un tarjeta', (done) => {
        superTest(app).post('/boards').send({'texto': 23}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar el sistema de actualización de tarjetas",() => {
    it('deberia de remplazar el tarjeta', (done) => {
        superTest(app).put(`/users/6387d9e14b98cb6ef6310ce7`).send({'text': 'columns'}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de remplazar el tarjeta', (done) => {
        superTest(app).put('/users/6387d9e14b98cb6ef6310ce7').send({'text': 321}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('deberia de actualizar el tarjeta', (done) => {
        superTest(app).patch('/users/6387d9e14b98cb6ef6310ce7').send({'text': 'columns2'}).expect(200)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('no deberia de actualizar el tarjeta', (done) => {
        superTest(app).patch('/users/6387d9e14b98cb6ef6310ce7').send({'name': 'text'}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar el sistema de destrucción de tarjetas",() => {
    it('deberia de borrar tarjeta', (done) => {
        superTest(app).delete('/users/6387d9e14b98cb6ef6310ce7').send().expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de borrar un tarjeta', (done) => {
        superTest(app).post('/users/6387d1572c3b6a2641d97574').send().expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});