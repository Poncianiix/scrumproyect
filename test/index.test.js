const superTest = require('supertest');

const app = require('../app');


//sentencia 

describe("probar el sistema de autenticación",() => {
    it('deberia de obtener un login con usuario y contrasena correoctos', (done) => {
        superTest(app).get('/login').send({'email': 'eric.aguimar@gmail.com', 'password': '123456'}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de obtener un login con usuario y contrasena incorreoctos', (done) => {
        superTest(app).get('/login').send({'email': 'eric.asguimar@gmail.com', 'password': '123456'}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});