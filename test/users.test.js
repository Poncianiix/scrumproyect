const superTest = require('supertest');

const app = require('../app');


//sentencia 

describe("probar el sistema de creación de usuarios",() => {
    it('deberia de crear usuario', (done) => {
        superTest(app).post('/users').send({'name': 'eric', 'lastName': 'Aguilar', 'email': 'eric.aguimar@gmail.com', 'password': '123456'}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de crear un usuario', (done) => {
        superTest(app).post('/users').send({'name': 'eric', 'lastName': 'Aguilar', 'email': 'eric.aguimar@gmail.com', 'password': 21}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar el sistema de actualización de usuarios",() => {
    it('deberia de remplazar usuario', (done) => {
        superTest(app).put(`/users/6387d1572c3b6a2641d9757c`).send({'name': 'eric', 'lastName': 'Aguilar', 'email': 'eric.aguimar@gmail.com', 'password': '123456'}).expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de remplazar usuario', (done) => {
        superTest(app).put('/users/6387d1572c3b6a2641d9757c').send({'name': 'eric', 'lastName': 'Aguilar', 'email': 'eric.aguimar@gmail.com', 'password': 21}).expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('deberia de actualizar usuario', (done) => {
        superTest(app).patch('/users/6387d1572c3b6a2641d9757c').send({'name': 'eric', 'lastName': 'Aguilar', 'email': 'eric.aguimar@gmail.com', 'password': '1234567'}).expect(200)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
    it('no deberia de actualizar usuario', (done) => {
        superTest(app).patch('/users/6387d1572c3b6a2641d9757c').send({'name': 'eric', 'lastName': 'Aguilar', 'email': 'eric.aguimar@gmail.com', 'password': '1234567'}).expect(200)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

describe("probar el sistema de destrucción de usuarios",() => {
    it('deberia de borrar usuario', (done) => {
        superTest(app).delete('/users/6387d1572c3b6a2641d9757c').send().expect(200)
        .end(function(error,res){
            if(error){
                done(error);
            }else{
                done();
            }
        });
    });
    it('no deberia de borrar un usuario', (done) => {
        superTest(app).post('/users/6387d1572c3b6a2641d97574').send().expect(403)
        .end(function(error,res){
            if(error){
                done(err);
            }else{
                done();
            }
        });
    })
});

