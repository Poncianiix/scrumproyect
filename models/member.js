const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _name: String,
    _lastname: String,
    _fechaNacimiento: Date,
    _curp: String,
    _rfc: String,
    _domicilio: String,
    _habilidades: {
        type: String,
        enum: ['Junior', 'Senior', 'Master']
    },
    _inicioSesion:{
        type: String,
        enum: ['Facebook', 'Instagram', 'Google', 'Sistema']
    },
    _rol: {
        type: String,
        enum: [ 'Desarrollador', 'Product Owner', 'Scrum Master']
    },

});

class Member{
    constructor(name,lastname,fechaNacimiento,curp,rfc,domicilio,habilidades,inicioSesion,rol){
        this._name = name;  
        this._lastname = lastname;
        this._fechaNacimiento = fechaNacimiento;
        this._curp = curp;
        this._rfc = rfc;
        this._domicilio = domicilio;
        this._habilidades = habilidades;
        this._inicioSesion = inicioSesion;
        this._rol = rol;
    }
    get name(){
        return this._name;
    }
    set name(name){
        this._name = name;
    }
    get lastname(){
        return this._lastname;
    }
    set lastname(lastname){
        this._lastname = lastname;
    }
    get fechaNacimiento(){
        return this._fechaNacimiento;
    }
    set fechaNacimiento(fechaNacimiento){
        this._fechaNacimiento = fechaNacimiento;
    }
    get curp(){
        return this._curp;
    }
    set curp(curp){
        this._curp = curp;
    }
    get rfc(){
        return this._rfc;
    }
    set rfc(rfc){
        this._rfc = rfc;
    }
    get domicilio(){
        return this._domicilio;
    }
    set domicilio(domicilio){
        this._domicilio = domicilio;
    }
    get habilidades(){
        return this._habilidades;
    }
    set habilidades(habilidades){
        this._habilidades = habilidades;
    }
    get inicioSesion(){
        return this._inicioSesion;
    }
    set inicioSesion(inicioSesion){
        this._inicioSesion = inicioSesion;
    }
    get rol(){
        return this._rol;
    }
    set rol(rol){
        this._rol = rol;
    }
}
schema.loadClass(Member);
module.exports = mongoose.model('Member', schema);