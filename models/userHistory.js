const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _nombre: String,
    _contexto: String,
    _rol: String,
    _beneficio: String,
    _prioridad: Number,
    _tamaño: Number,
    _evento: String,
    _resultado: String,
    _status: Boolean,
    _columna: Number,

});

class UserHistory{
    constructor(nombre,contexto,rol,beneficio,prioridad,tamaño,evento,resultado,status,columna){
        this._nombre = nombre;  
        this._contexto = contexto;
        this._rol = rol;
        this._beneficio = beneficio;
        this._prioridad = prioridad;
        this._tamaño = tamaño;
        this._evento = evento;
        this._resultado = resultado;
        this._status = status;
        this._columna = columna;
    }
    get nombre(){
        return this._nombre;
    }
    set nombre(nombre){
        this._nombre = nombre;
    }
    get contexto(){
        return this._contexto;
    }
    set contexto(contexto){
        this._contexto = contexto;
    }
    get rol(){
        return this._rol;
    }
    set rol(rol){
        this._rol = rol;
    }
    get beneficio(){
        return this._beneficio;
    }
    set beneficio(beneficio){
        this._beneficio = beneficio;
    }
    get prioridad(){
        return this._prioridad;
    }
    set prioridad(prioridad){
        this._prioridad = prioridad;
    }
    get tamaño(){
        return this._tamaño;
    }
    set tamaño(tamaño){
        this._tamaño = tamaño;
    }
    get evento(){
        return this._evento;
    }
    set evento(evento){
        this._evento = evento;
    }
    get resultado(){
        return this._resultado;
    }
    set resultado(resultado){
        this._resultado = resultado;
    }
    get status(){
        return this._status;
    }
    set status(status){
        this._status = status;
    }
    get columna(){
        return this._columna;
    }
    set columna(columna){
        this._columna = columna;
    }
}
schema.loadClass(UserHistory);
module.exports = mongoose.model('UserHistory', schema);