const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _name: String,
    _fechaSolictud: Date,
    _fechaArranque: Date,
    _proyectManager: String,
    _productOwner: String,
    _descripcion: String,
    _status: Boolean,
    _equipoDesarrollo: [{type: mongoose.Schema.Types.ObjectId, ref: 'Member'}],

});

class Proyectfile{
    constructor(name,fechaSolictud,fechaArranque,proyectManager,productOwner,descripcion,status,equipoDesarrollo){
        this._name = name;  
        this._fechaSolictud = fechaSolictud;
        this._fechaArranque = fechaArranque;
        this._proyectManager = proyectManager;
        this._productOwner = productOwner;
        this._descripcion = descripcion;
        this._status = status;
        this._equipoDesarrollo = equipoDesarrollo;
    }
    get name(){
        return this._name;
    }
    set name(name){
        this._name = name;
    }
    get fechaSolictud(){
        return this._fechaSolictud;
    }
    set fechaSolictud(fechaSolictud){
        this._fechaSolictud = fechaSolictud;
    }
    get fechaArranque(){
        return this._fechaArranque;
    }
    set fechaArranque(fechaArranque){
        this._fechaArranque = fechaArranque;
    }
    get proyectManager(){
        return this._proyectManager;
    }
    set proyectManager(proyectManager){
        this._proyectManager = proyectManager;
    }
    get productOwner(){
        return this._productOwner;
    }
    set productOwner(productOwner){
        this._productOwner = productOwner;
    }
    get descripcion(){
        return this._descripcion;
    }
    set descripcion(descripcion){
        this._descripcion = descripcion;
    }
    get status(){
        return this._status;
    }
    set status(status){
        this._status = status;
    }
    get equipoDesarrollo(){
        return this._equipoDesarrollo;
    }
    set equipoDesarrollo(equipoDesarrollo){
        this._equipoDesarrollo = equipoDesarrollo;
    }
}
schema.loadClass(Proyectfile);
module.exports = mongoose.model('Proyectfile', schema);