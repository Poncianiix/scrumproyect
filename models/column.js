const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _titulo: String,

});

class Column{
    constructor(titulo){
        this._titulo = titulo;  
    }
    get titulo(){
        return this._titulo;
    }
    set titulo(titulo){
        this._titulo = titulo;
    }
}
schema.loadClass(Column);
module.exports = mongoose.model('Column', schema);