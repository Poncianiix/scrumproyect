const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _name: String,
    _columns: [{type: mongoose.Schema.Types.ObjectId, ref: 'Column'}],
  
});

class Board{
    constructor(name,columns){
        this._name = name;  
        this._columns = columns;
    }
    get name(){
        return this._name;
    }
    set name(name){
        this._name = name;
    }
    get columns(){
        return this._columns;
    }
    set columns(columns){
        this._columns = columns;
    }
}
schema.loadClass(Board);
module.exports = mongoose.model('Board', schema);