const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _texto: String,

});

class card{
    constructor(texto){
        this._texto = texto;  
    }
    get texto(){
        return this._texto;
    }
    set texto(texto){
        this._texto = texto;
    }
}
schema.loadClass(card);
module.exports = mongoose.model('card', schema);