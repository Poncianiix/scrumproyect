const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _titulo: String,
    _cards: [{type: mongoose.Schema.Types.ObjectId, ref: 'Card'}],

});

class retrospective{
    constructor(titulo,cards){
        this._titulo = titulo;  
        this._cards = cards;
    }
    get titulo(){
        return this._titulo;
    }
    set titulo(titulo){
        this._titulo = titulo;
    }
    get cards(){
        return this._cards;
    }
    set cards(cards){
        this._cards = cards;
    }
}
schema.loadClass(retrospective);
module.exports = mongoose.model('retrospective', schema);