const express = require('express');
const Member = require('../models/member');





function list(req, res, next) {
    Member.find().then(obj => res.status(200).json({
        message: res.__('ok.listaMembers'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaMembers'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    Member.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getMember'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getMember'),
        error: err
    }));
}


function create(req, res, next) {

    const name = req.body.name;
    const lastName = req.body.lastName;
    const fechaNacimiento = req.body.fechaNacimiento;
    const curp = req.body.curp;
    const rfc = req.body.rfc;
    const domicilio = req.body.domicilio;
    const habilidades = req.body.habilidades;
    const inicioSesion = req.body.inicioSesion;
    const rol = req.body.rol;


    let member = new Member({
        name: name,
        lastName:lastName,
        fechaNacimiento:fechaNacimiento,
        curp:curp,
        rfc:rfc,
        domicilio:domicilio,
        habilidades:habilidades,
        inicioSesion:inicioSesion,
        rol:rol

    });
    member.save().then(obj => res.status(200).json({
        message: res.__('ok.createMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createMember'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
    let lastName = req.body.lastName ? req.body.lastName : " ";
    let fechaNacimiento = req.body.fechaNacimiento ? req.body.fechaNacimiento : " ";
    let curp = req.body.curp ? req.body.curp : " ";
    let rfc = req.body.rfc ? req.body.rfc : " ";
    let domicilio = req.body.domicilio ? req.body.domicilio : " ";
    let habilidades = req.body.habilidades ? req.body.habilidades : " ";
    let inicioSesion = req.body.inicioSesion ? req.body.inicioSesion : " ";
    let rol = req.body.rol ? req.body.rol : " ";

    
    
    let member = new Object({
        _name :name,
        _lastName:lastName,
        _fechaNacimiento:fechaNacimiento,
        _curp:curp,
        _rfc:rfc,
        _domicilio:domicilio,
        _habilidades:habilidades,
        _inicioSesion:inicioSesion,
        _rol:rol

    });

    Member.findOneAndUpdate({_id:id},member, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateMember'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let fechaNacimiento = req.body.fechaNacimiento;
    let curp = req.body.curp;
    let rfc = req.body.rfc;
    let domicilio = req.body.domicilio;
    let habilidades = req.body.habilidades;
    let inicioSesion = req.body.inicioSesion;
    let rol = req.body.rol;

 
    
    let member = new Object();

    if(name){
        member._name = name;
    }
    if(lastName){
        member._lastName = lastName;
    }
    if(fechaNacimiento){
        member._fechaNacimiento = fechaNacimiento;
    }
    if(curp){
        member._curp = curp;
    }
    if(rfc){
        member._rfc = rfc;
    }
    if(domicilio){
        member._domicilio = domicilio;
    }
    if(habilidades){
        member._habilidades = habilidades;
    }
    if(inicioSesion){
        member._inicioSesion = inicioSesion;
    }
    if(rol){
        member._rol = rol;
    }
    

    Member.findOneAndUpdate({_id:id},member).then(obj => res.status(200).json({
        message: res.__('ok.updateMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateMember'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Member.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteMember'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}