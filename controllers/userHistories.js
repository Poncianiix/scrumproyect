const express = require('express');
const UserHistory = require('../models/userHistory');


function list(req, res, next) {
    UserHistory.find().then(obj => res.status(200).json({
        message: res.__('ok.listaUH'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaUH'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    UserHistory.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getUH'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getUH'),
        error: err
    }));
}


 function create(req, res, next) {

    const nombre = req.body.nombre;
    const contexto = req.body.contexto;
    const rol = req.body.rol;
    const beneficio = req.body.beneficio;
    const prioridad = req.body.prioridad;
    const tamaño = req.body.tamaño;
    const evento = req.body.evento;
    const resultado = req.body.resultado;
    const status = req.body.status;
    const columna = req.body.columna;

   
 

    let userHistory = new UserHistory({
        nombre: nombre,
        contexto:contexto,
        rol:rol,
        beneficio:beneficio,
        prioridad:prioridad,
        tamaño:tamaño,
        evento:evento,
        resultado:resultado,
        status:status,
        columna:columna,
        
    });
    userHistory.save().then(obj => res.status(200).json({
        message: res.__('ok.createUH'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createUH'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let nombre = req.body.nombre ? req.body.nombre : " ";
    let contexto = req.body.contexto ? req.body.contexto : " ";
    let rol = req.body.rol ? req.body.rol : " ";
    let beneficio = req.body.beneficio ? req.body.beneficio : " ";
    let prioridad = req.body.prioridad ? req.body.prioridad : " ";
    let tamaño = req.body.tamaño ? req.body.tamaño : " ";
    let evento = req.body.evento ? req.body.evento : " ";
    let resultado = req.body.resultado ? req.body.resultado : " ";
    let status = req.body.status ? req.body.status : " ";
    let columna = req.body.columna ? req.body.columna : " ";
    
    let userHistory = new Object({
        _nombre :nombre,
        _contexto :contexto,
        _rol :rol,
        _beneficio :beneficio,
        _prioridad :prioridad,
        _tamaño :tamaño,
        _evento :evento,
        _resultado :resultado,
        _status :status,
        _columna :columna,

    });

    UserHistory.findOneAndUpdate({_id:id},userHistory, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateUH'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateUH'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let nombre = req.body.nombre;
    let contexto = req.body.contexto;
    let rol = req.body.rol;
    let beneficio = req.body.beneficio;
    let prioridad = req.body.prioridad;
    let tamaño = req.body.tamaño;
    let evento = req.body.evento;
    let resultado = req.body.resultado;
    let status = req.body.status;
    let columna = req.body.columna;

   
    let userHistory = new Object();

    if(nombre){
        userHistory._nombre = nombre;
    }
    if(contexto){
        userHistory._contexto = contexto;
    }
    if(rol){
        userHistory._rol = rol;
    }
    if(beneficio){
        userHistory._beneficio = beneficio;
    }
    if(prioridad){
        userHistory._prioridad = prioridad;
    }
    if(tamaño){
        userHistory._tamaño = tamaño;
    }
    if(evento){
        userHistory._evento = evento;
    }
    if(resultado){
        userHistory._resultado = resultado;
    }
    if(status){
        userHistory._status = status;
    }
    if(columna){
        userHistory._columna = columna;
    }

    
   
    

    UserHistory.findOneAndUpdate({_id:id},userHistory).then(obj => res.status(200).json({
        message: res.__('ok.updateUH'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateUH'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    UserHistory.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteUH'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteUH'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}