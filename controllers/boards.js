const express = require('express');
const Board = require('../models/board');




function list(req, res, next) {
    Board.find().then(obj => res.status(200).json({
        message: res.__('ok.listaBoard'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaBoard'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    Board.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getBoard'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getBoard'),
        error: err
    }));
}


 function create(req, res, next) {

    const name = req.body.name;
    let columns = req.body.columns;
    let board = new Board({
        name: name,
        colums: columns,
    });
    board.save().then(obj => res.status(200).json({
        message: res.__('ok.createBoard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createBoard'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
      
    let board = new Object({
        _name :name,
    });

    Board.findOneAndUpdate({_id:id},board, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateBoard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateBoard'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;

    let board = new Object();

    if(name){
        board._name = name;
    }

    Board.findOneAndUpdate({_id:id},board).then(obj => res.status(200).json({
        message: res.__('ok.updateBoard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateBoard'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Board.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteBoard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteBoard'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}