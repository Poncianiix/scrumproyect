const express = require('express');
const User = require('../models/users');

const bcrypt = require('bcrypt');



function list(req, res, next) {
    User.find().then(obj => res.status(200).json({
        message: res.__('ok.listaUsuario'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaUsuario'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    User.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getUsuario'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getUsuario'),
        error: err
    }));
}


async function create(req, res, next) {

    const {name, lastName,email,password} = req.body;


    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password,salt);

    const user = new User({
        name,
        lastName,
        password: passwordHash,
        email,
        salt: salt
    })

    user.save().then(obj => res.status(200).json({
        message: res.__('ok.createUsuario'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createUsuario'),
        obj: err
    }));
}


async function replace(req, res, next) {

    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
    let lastName = req.body.lastName ? req.body.lastName : " ";
    let email = req.body.email ? req.body.email : " ";
    let password = req.body.password ? req.body.password : " ";

    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password,salt);

    let User = new Object({
        _name :name,
        _lastName :lastName,
        _email :email,
        _password :passwordHash,
        _salt: salt
    });

    User.findOneAndUpdate({_id:id},User, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateUsuario'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateUsuario'),
        obj: err
    }));
}
async function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let email = req.body.email;
    let password = req.body.password;

    const salt = await bcrypt.genSalt(10);
    const passwordHash = await bcrypt.hash(password,salt);

    let User = new Object({
        _name :name,
        _lastName :lastName,
        _email :email,
        _password :passwordHash,
        _salt: salt
    });
    User.findOneAndUpdate({_id:id},User).then(obj => res.status(200).json({
        message: res.__('ok.updateUsuario'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateUsuario'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    User.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteUsuario'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteUsuario'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}