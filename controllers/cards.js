const express = require('express');
const Card = require('../models/card');





function list(req, res, next) {
    Card.find().then(obj => res.status(200).json({
        message: res.__('ok.listaCards'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaCards'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    Card.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getCard'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getCard'),
        error: err
    }));
}


function create(req, res, next) {

    const texto = req.body.texto;

    let card = new Card({
        texto: texto,
    });
    card.save().then(obj => res.status(200).json({
        message: res.__('ok.createCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createCard'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let texto = req.body.texto ? req.body.texto : " ";
      
    let card = new Object({
        _texto :texto,
    });

    Card.findOneAndUpdate({_id:id},card, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateCard'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let texto = req.body.texto;

    let card = new Object();

    if(texto){
        card._texto = texto;
    }

    Card.findOneAndUpdate({_id:id},card).then(obj => res.status(200).json({
        message: res.__('ok.updateCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateCard'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Card.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteCard'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}