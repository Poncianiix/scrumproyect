const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/users');
const jwt = require('jsonwebtoken');
const config = require('config')
function home(req, res, next) {
    res.render('index', { title: 'Express' });
}

function login(req,res,next){
    const {email,password} = req.body;
    User.findOne({'_email':email}).select('_password _salt').then((user) => {
        if(user){
            console.log(user);
            bcrypt.hash(password,user.salt,(err, hash) => {
                if(err){
                    res.status(403).json({
                        message: res.__('bad.login'),
                        obj: err
                        
                    })
                    return;
                }
                if(hash === user.password){
                    const jwtKey = config.get('secret.get');

                    res.status(200).json({
                        message: res.__('ok.login'),
                        obj: jwt.sign({exp: Math.floor(Date.now()/1000)+3600},jwtKey)
                    })
                    return;
                }else{
                    res.status(403).json({
                        
                        message: res.__('bad.login'),
                        obj: null
                    })
                    return;
                }
            })
        }else {
            res.status(403).json({
                message: res.__('bad.login'),
                obj: null
            })
            return;
        }
    }).catch(err => {
        res.status(403).json({
            message: res.__('bad.login'),
            obj: null
        })
        return;
    });

}

module.exports = {home, login};