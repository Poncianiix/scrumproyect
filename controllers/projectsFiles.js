const express = require('express');
const Proyectfile = require('../models/proyectfile');
const Member = require('../models/member');


function list(req, res, next) {
    Proyectfile.find().then(obj => res.status(200).json({
        message: res.__('ok.listaPF'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaPF'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    Proyectfile.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getPF'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getPF'),
        error: err
    }));
}


async function create(req, res, next) {

    const name = req.body.name;
    const fechaSolictud = req.body.fechaSolictud;
    const fechaArranque = req.body.fechaArranque;
    const proyectManager = req.body.proyectManager;
    const productOwner = req.body.productOwner;
    const descripcion = req.body.descripcion;
    const status = req.body.status;
    let member = await Member.findOne({_id:cardId});
 

    let proyectfile = new Proyectfile({
        name: name,
        fechaSolictud:fechaSolictud,
        fechaArranque:fechaArranque,
        proyectManager:proyectManager,
        productOwner:productOwner,
        descripcion:descripcion,
        status:status,
        member
    });
    proyectfile.save().then(obj => res.status(200).json({
        message: res.__('ok.createPF'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createPF'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name : " ";
    let fechaSolictud = req.body.fechaSolictud ? req.body.fechaSolictud : " ";
    let fechaArranque = req.body.fechaArranque ? req.body.fechaArranque : " ";
    let proyectManager = req.body.proyectManager ? req.body.proyectManager : " ";
    let productOwner = req.body.productOwner ? req.body.productOwner : " ";
    let descripcion = req.body.descripcion ? req.body.descripcion : " ";
    let status = req.body.status ? req.body.status : " ";
   
    
    
    let proyectfile = new Object({
        _name :name,
        _fechaSolictud:fechaSolictud,
        _fechaArranque:fechaArranque,
        _proyectManager:proyectManager,
        _productOwner:productOwner,
        _descripcion:descripcion,
        _status:status,
     

    });

    Proyectfile.findOneAndUpdate({_id:id},proyectfile, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updatePF'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updatePF'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let name = req.body.name;
    let fechaSolictud = req.body.fechaSolictud;
    let fechaArranque = req.body.fechaArranque;
    let proyectManager = req.body.proyectManager;
    let productOwner = req.body.productOwner;
    let descripcion = req.body.descripcion;
    let status = req.body.status;



 
    
    let proyectfile = new Object();

    if(name){
        proyectfile._name = name;
    }
    if(fechaSolictud){
        proyectfile._fechaSolictud = fechaSolictud;
    }
    if(fechaArranque){
        proyectfile._fechaArranque = fechaArranque;
    }
    if(proyectManager){
        proyectfile._proyectManager = proyectManager;
    }
    if(productOwner){
        proyectfile._productOwner = productOwner;
    }
    if(descripcion){
        proyectfile._descripcion = descripcion;
    }
    if(status){
        proyectfile._status = status;
    }
   
    

    Proyectfile.findOneAndUpdate({_id:id},proyectfile).then(obj => res.status(200).json({
        message: res.__('ok.updatePF'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updatePF'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Proyectfile.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deletePF'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deletePF'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}