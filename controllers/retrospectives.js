const express = require('express');
const Retrospective = require('../models/restrospective');
const Card = require('../models/card');




function list(req, res, next) {
    Retrospective.find().then(obj => res.status(200).json({
        message: res.__('ok.listaRetrospectiva'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaRetrospectiva'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    Retrospective.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getRetrospectiva'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getRetrospectiva'),
        error: err
    }));
}


async function create(req, res, next) {

    const titulo = req.body.titulo;
    const cardId = req.body.cardId;
    let card = await Card.findOne({_id:cardId});
    let retrospective = new Retrospective({
        titulo: titulo,
        card,
    });
    retrospective.save().then(obj => res.status(200).json({
        message: res.__('ok.createCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createCard'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let titulo = req.body.titulo ? req.body.titulo : " ";
      
    let retrospective = new Object({
        _titulo :titulo,
    });

    Retrospective.findOneAndUpdate({_id:id},retrospective, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.updateCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateCard'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let titulo = req.body.titulo;

    let retrospective = new Object();

    if(titulo){
        retrospective._titulo = titulo;
    }

    Retrospective.findOneAndUpdate({_id:id},retrospective).then(obj => res.status(200).json({
        message: res.__('ok.updateCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.updateCard'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Retrospective.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.deleteCard'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.deleteCard'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}