const express = require('express');
const Column = require('../models/column');





function list(req, res, next) {
    Column.find().then(obj => res.status(200).json({
        message: res.__('ok.listaColumnas'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.listaColumnas'),
        error: err
    }));
}
function index(req, res, next) {
    const id = req.params;
    Column.findOne({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.getColumna'),
        createdMember: obj
    })).catch(err => res.status(500).json({
        message: res.__('bad.getColumna'),
        error: err
    }));
}


function create(req, res, next) {

    const titulo = req.body.titulo;

    let column = new Column({
        titulo: titulo,
    });
    column.save().then(obj => res.status(200).json({
        message: res.__('ok.createMembers'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.createMembers'),
        obj: err
    }));
}


function replace(req, res, next) {
    const id = req.params.id;
    let titulo = req.body.titulo ? req.body.titulo : " ";
      
    let column = new Object({
        _titulo :titulo,
    });

    Column.findOneAndUpdate({_id:id},column, {new: true}).then(obj => res.status(200).json({
        message: res.__('ok.actualizarMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.actualizarMember'),
        obj: err
    }));
}
function update(req, res, next) {
    const id = req.params.id;
    let titulo = req.body.titulo;

    let column = new Object();

    if(titulo){
        column._titulo = titulo;
    }

    Column.findOneAndUpdate({_id:id},column).then(obj => res.status(200).json({
        message: res.__('ok.actualizarMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.actualizarMember'),
        obj: err
    }));
}
function destroy(req, res, next) {
    const id = req.params.id;
    Column.remove({_id:id}).then(obj => res.status(200).json({
        message: res.__('ok.eliminarMember'),
        obj: obj
    }))
    .catch(err => res.status(500).json({
        message: res.__('bad.eliminarMember'),
        obj: err
    }));
}


module.exports = {list,index,create,replace,update,destroy}