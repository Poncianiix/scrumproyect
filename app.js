const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const {expressjwt} = require('express-jwt');
const i18n = require('i18n');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const membersRouter = require('./routes/members');
const projectFilesRouter = require('./routes/projectfiles');
const boardRouter = require('./routes/boards');
const columnRouter = require('./routes/columns');
const userHistoriesRouter = require('./routes/userHistories');
const RetrospectivesRouter = require('./routes/Retrospectives');
const cardRouter = require('./routes/cards');
const mongoose = require('mongoose');
const config = require('config')


//const uri ="mongodb://localhost:27017/videoClub";
const uri = config.get('dbChain');
mongoose.connect(uri);
const db = mongoose.connection;

const app = express();


db.on('open',()=>{
	console.log("Conexion correcta");
});
db.on('error',(err)=> {
	console.log("Error al conectar",err);
});

i18n.configure({
  locales:['es','en'],
  cookie: 'language',
  directory: `${__dirname}/locales`

});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.init);

const jwtkey = config.get("secret.key");
app.use(expressjwt({secret: jwtkey,algorithms: ['HS256']})
   .unless({path: ['/login']}));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/members',membersRouter);
app.use('/projectFiles',projectFilesRouter);
app.use('/boards', boardRouter );
app.use('/columns',columnRouter);
app.use('/userHistories',userHistoriesRouter);
app.use('/retrospectives',RetrospectivesRouter);
app.use('/cards',cardRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
