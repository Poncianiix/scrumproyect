# Proyecto de scrum

# Imagen en docker

docker pull poncianix/proyectoscrum

# Explicación diagrama de clases:
En el diagrama de clases creamos en total 10 clases, aunque la clase columna hereda a 3 clases product backlog, release backlog y sprint backlog. La clase retrospectiva depende de que exista un sprint backlog. Por último habilidad, método para iniciar sesión y el rol son de tipo enum.


# Explicación diagrama de interacción:

El administrador crea un proyecto, desde ese proyecto es capaz de crear usuarios que pueden participar en el tablero. Por consecuencia, el administrador tiene que crear un tablero como mínimo.
Cuando se crea un tablero, se tienen que crear columnas, que nos van a servir para product_backlog, release_backlog y sprint_backlog. El usuario crea una historia y la envía a product_backlog, en donde se decide si estas se aprueban o no. Cuando un product_backlog se aprueba, es enviado a release_backlog. Cuando se está en release_backlog, se realiza una estimación de tiempo. En el sprint_backlog, se ven los avances en retrospectiva. Tanto en release_backlog como en sprint_backlog se puede generar un burndown chart.

# Nota

Los diagramas estan en la carpeta de digramas
